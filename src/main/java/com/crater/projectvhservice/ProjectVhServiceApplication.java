package com.crater.projectvhservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectVhServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectVhServiceApplication.class, args);
    }

}
